## AntifaGLUG

Gitlab page de Antifa GNU/Linux Users Group Buenos Aires.

Misma lógica que otras landings que nos sirven para acomodar dominios y linkear sitios. Hasta tanto tengamos una mejor propuesta/idea para una landing, app, blog u otro tipo de contenido que pueda ser superador, dejamos una versión simple en html plano y css.

En la landing linkeamos a:

- Wiki Antifa
- Código de conducta
- Main rlab
- Manifiesto rlab

Enlace gitlab page: [https://antifaglug.org/](https://antifaglug.org)